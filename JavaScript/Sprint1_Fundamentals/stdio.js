// Print simple string
console.log("Hello, World!")

// Print string variables and constants
let eventName = "hackETHon"
console.log("Name of the event is: ${eventName}")

var timeLeft= "much"
console.log("At this moment we have ${timeLeft time}")

// Print with string interpolation
let π = 3.14159
console.log("Number with string interpolation: ${π}")


var readline = require('readline');

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout

});

rl.question(">>What's your name?  ", function(answer) {
   console.log("Hello " + answer);
   rl.close();
});