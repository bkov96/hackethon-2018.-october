//export and import

// circle.js
export const PI = 3.14;
export default function area(radius) {
 return PI * radius * radius;
}

//here
import circleArea from './circle';
console.log(circleArea(4));

//Importing named members from another module

//Given that the module from the Defining a Module section exists in the file test.js, you can import from that
//module and use its exported members:
import {doSomething, MyClass, PI} from './test'
doSomething()
const mine = new MyClass()
mine.test()
console.log(PI)
//The somethingPrivate() method was not exported from the test module, so attempting to import it will fail:
import {somethingPrivate} from './test'
somethingPrivate()
// Importing an entire module
import * as test from './test'
test.doSomething()

