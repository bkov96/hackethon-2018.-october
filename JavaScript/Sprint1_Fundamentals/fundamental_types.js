var myInteger = 12; // 32-bit number (from -2,147,483,648 to 2,147,483,647)
var myLong = 9310141419482; // 64-bit number (from -9,223,372,036,854,775,808 to
//9,223,372,036,854,775,807)
var myFloat = 5.5; // 32-bit floating-point number (decimal)
var myDouble = 9310141419482.22; // 64-bit floating-point number
var myBoolean = true; // 1-bit true/false (0 or 1)
var myBoolean2 = false;
var myNotANumber = NaN;
var NaN_Example = 0/0; // NaN: Division by Zero is not possible
var notDefined; // undefined: we didn't define it to anything yet
var myNull = null; // null
var myArray = []; // empty array
var favoriteFruits = ["apple", "orange", "strawberry"];


//The Number constructor has some built in constants that can be usefule
Number.MAX_VALUE; // 1.7976931348623157e+308
Number.MAX_SAFE_INTEGER; // 9007199254740991
Number.MIN_VALUE; // 5e-324
Number.MIN_SAFE_INTEGER; // -9007199254740991
Number.EPSILON; // 0.0000000000000002220446049250313
Number.POSITIVE_INFINITY; // Infinity
Number.NEGATIVE_INFINITY; // -Infinity
Number.NaN; // NaN


// 1. Strings
// typeof "String" or
// typeof Date(2011,01,01)
// "string"
// 2. Numbers
// typeof 42
// "number"
// 3. Bool
// typeof true (valid values true and false)
// "boolean"
// 4. Object
// typeof {} or
// typeof [] or
// typeof null or
// typeof /aaa/ or
// typeof Error()
// "object"
// 5. Function
// typeof function(){}
// "function"
// 6. Undefined
// var var1; typeof var1
// "undefined"