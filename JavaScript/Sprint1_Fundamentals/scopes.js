// --- NAMESPACES
function foo() {
    var a = 'hello';
    function bar() {
    var b = 'world';
    console.log(a); // => 'hello'
    console.log(b); // => 'world'
    }
    console.log(a); // => 'hello'
    console.log(b); // reference error
   }
   console.log(a); // reference error
   console.log(b); // reference error

//another one example
   function foo() {
    const a = true;
    function bar() {
    const a = false; // different variable
    console.log(a); // false
    }
    const a = false; // SyntaxError
    a = false; // TypeError
    console.log(a); // true
   }
   