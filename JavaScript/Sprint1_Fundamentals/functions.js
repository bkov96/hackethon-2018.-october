//void function
function foo() {
    var a = 'hello';
    console.log(a); // => 'hello'
   }
// Immediately Invoked Function Expressions
var example = (function() {
 return 42;
}());
console.log(example); // => 42

//named functions
var namedSum = function sum (a, b) { // named
    return a + b;
   }
   var anonSum = function (a, b) { // anonymous
    return a + b;
   }
   namedSum(1, 3);
   anonSum(1, 3);
// Functions with Parameters
   var anonSum = function (a, b) { 
    return a + b;
   }
   namedSum(1, 3);