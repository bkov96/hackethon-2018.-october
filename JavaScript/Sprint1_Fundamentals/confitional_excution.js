let condition = true

// Simple if statement, brackets cannot be missed
if (condition) {
    // condition met
}

// if, else execution
if (condition) {
    // condition met
}
else {
    // condition didn't met
}

// if, else-if, else execution
if (condition) {
    // condition met
}
else if (!condition) {
    // condition didn't met
}
else {
    // cannot be reached
}

// swich-case
/* Break is needed in cases if there is
   any executable command in it */
let direction = 1
switch(direction)
{
    case 1:
        break
    case 2:
        break
    case 3:
        break
    case 4:
        break
    default:
}