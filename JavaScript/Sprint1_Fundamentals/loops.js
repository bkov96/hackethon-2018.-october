// --- FOR LOOP

// Iterating For-of array
let myArray = [1, 2, 3, 4]
for (let value of myArray) {
 let twoValue = value * 2
 console.log("2 * value is: %d", twoValue)
}

// Iterating For-in array
var existing = { a: 1, b: 2, c: 3 }
var clone = {}
for (var prop in existing) {
 if (existing.hasOwnProperty(prop)) {
 clone[prop] = existing[prop]
 }
}


// Iterating over an array or set
let loopwithfilter = [1, 2, 3, 4, 5]
for (var value in loopwithfilter){
    console.log("value is: %d", value)
}

// For-in loop with filtering over array
let loopwithfilter = [1, 2, 3, 4, 5].filter(value => value > 2)
for (var value in loopwithfilter){
    console.log("value is: %d", value)
}

// --- WHILE LOOP
var i = 0;
while (i < 100) {
    console.log(i);
 	i++;
}

// --- DO-WHILE
var i = 0;
do {
    i++;
    console.log(i);
} while (i < 100);