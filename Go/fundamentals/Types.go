package fundamentals

import (
	"fmt"
	"unsafe"
)

// DeclarationsWithTypes - used ([A-Za-z0-9]+):\s+.*$
func DeclarationsWithTypes() {
	var _ string = "Hello World"
	var _ bool = true
	var x2 int = -2
	var _ int8 = int8(2)
	var _ int16 = -2
	var _ int32 = -2
	var _ int64 = -2
	var _ uint = ^uint(0)   // maximum uint
	var x uint8 = ^uint8(2) // maximum uint8 - 2 = 253
	fmt.Println(x)
	var (
		_ uint16 = 2
		_ uint32 = 2
		_ uint64 = 2
	)
	var x111 unsafe.Pointer = nil
	var x12 uintptr = uintptr(x111)
	var x122 *int = &x2
	var _ *int = nil
	var _ float32 = 2.2
	var _ float64 = float64(*x122)

	//func complex(r, i FloatType) ComplexType
	var _ complex64 = 6 + 7i
	var _ complex128 = complex(6, 7)
	var _ string = "Hello World"
	var _ unsafe.Pointer = unsafe.Pointer(x12)
	//	Untyped Constants
	const x19 = true
	const x20 = 2
	const x21 = "Hello World"
	const x22 = 2.2
	const x23 = 6 + 7i
	const x24 = "Hello World"
	// Typed Constants
	const _ bool = true
	const _ int = 2
	const _ string = "Hello World"
}

// DeclarationWithAuto - used ([A-Za-z0-9]+):\s+.*$
func DeclarationWithAuto() {
	a := "Hello World"
	const b = 2.2
	_, x := a, b
	fmt.Println(x)
}

// Numbers - amen
func Numbers() {
	fmt.Println("1 + 1 =", 1+1)
	fmt.Println("1 + 1 =", 1.0+1.0)
}

// Strings - amen
func Strings() {
	fmt.Println(len("Hello World")) //	11
	fmt.Println("Hello World"[1])   //	101 (e = 101)
	fmt.Println("Hello " + "World") //	Hello World
}

// Booleans - amen
func Booleans() {
	fmt.Println(true && true)  //true
	fmt.Println(true && false) //false
	fmt.Println(true || true)  //true
	fmt.Println(true || false) //true
	fmt.Println(!true)         //false
}
