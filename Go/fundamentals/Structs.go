package fundamentals

import (
	"fmt"
	"math"
)

// Circle - struct type
type Circle struct {
	x float64
	y float64
	r float64
}

// (c *Circle) is the receiver object
func (c *Circle) area() float64 {
	return math.Pi * c.r * c.r
}

// Rect ...
type Rect struct {
	a, b float64
}

// Shape ...
type Shape interface {
	area() float64
}

// MultiShape ...
type MultiShape struct {
	shapes []Shape
}

func totalArea(shapes ...Shape) float64 {
	var area float64
	for _, s := range shapes {
		area += s.area()
	}
	return area
}

// LightningStructMe - it's true == false
func LightningStructMe() {
	var c Circle
	// (*Rect)
	rect := new(Rect)

	c2 := Circle{x: 0, y: 0, r: 5}
	c3 := Circle{3, 5, 10}

	c.x = 10
	c.y = 5

	(*rect).a = 10
	(*rect).b = 5

	fmt.Printf("Circle2 area is %f\n", c2.area())
	fmt.Printf("Circle3 area is %f\n", c3.area())
	fmt.Println("Total area is: ", totalArea(&c2, &c3))
}

// Person ...
type Person struct {
	Name string
}

// Talk ...
func (p *Person) Talk() {
	fmt.Println("Hi, my name is", p.Name)
}

// AndroidComposition ...
type AndroidComposition struct {
	Person Person
	Model  string
}

// AndroidEmbedded ...
type AndroidEmbedded struct {
	Person
	Model string
}

// LetsMixCwithPP ...
func LetsMixCwithPP() {
	a := new(AndroidEmbedded)
	a.Name = "Jason"
	a.Person.Talk()
	a.Talk()
}
