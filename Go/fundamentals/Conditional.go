package fundamentals

import (
	"fmt"
)

func ifYouWish(i int) {
	if i%2 == 0 {
		fmt.Println("divisible by 2")
	} else if i%3 == 0 {
		fmt.Println("divisible by 3")
	} else if i%4 == 0 {
		fmt.Println("divisible by 4")
	}
}

func sWitch(i int) {
	switch i {
	case 0:
		fmt.Println("Zero")
	case 1:
		fmt.Println("One")
	case 2:
		fmt.Println("Two")
	case 3:
		fmt.Println("Three")
	case 4:
		fmt.Println("Four")
	case 5:
		fmt.Println("Five")
	default:
		fmt.Println("Unknown Number")
	}
}

// Conditionals ...
func Conditionals() {
	ifYouWish(2)
	sWitch(4)
}
