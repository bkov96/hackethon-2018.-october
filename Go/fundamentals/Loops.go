// You can edit this code!
// Click here and start typing.

package fundamentals

import (
	"fmt"
)

func forNarnia() {
	var array = []int{}
	for i := 1; i < 10; i++ {
		array = append(array, i)
	}
	fmt.Println("For loop ended: ", array)
}

func forBreaks() {
	// Like a C for(;;)
	i := 0
	// while (1) { break }
	for {
		i++
		if i > 9 {
			break
		}
	}
	fmt.Println("For with break ended: ", i)
}

type condition func() bool

func while(cond condition, block func()) {
	// this is the real 3rd type of for
	for cond() {
		block()
	}
}

// Loops - Very sad indeed
func Loops() {
	forNarnia()
	forBreaks()

	i := 0
	while(func() bool { return i < 10 }, func() {
		i++
	})
	fmt.Println("While loop (for) ended: ", i)

}
