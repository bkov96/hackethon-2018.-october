package fundamentals

import (
	"fmt"
)

//	Some comments

/*
	Some very long latin context.

	Lorem ipsum dolor sit amet, consectetur adipiscing elit,
	sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
	Lut enim ad minim veniam, quis nostrud exercitation ullamco laboris
	nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
	reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
	 Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officiae deserunt
	 mollit anim id est laborum.
*/

// NeedYourComment - needs serious commenting
func NeedYourComment() {
	fmt.Println("needYourComment called")
}
