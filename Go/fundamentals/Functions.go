package fundamentals

import (
	"fmt"
)

func multiReturn() (int, int) {
	return 5, 6
}

func variadicAdder(args ...int) int {
	total := 0
	for _, v := range args {
		total += v
	}
	return total
}

func closure() {
	accum := 0
	add := func() {
		accum++
	}
	for accum < 10 {
		add()
	}
	fmt.Println("Closure add result: ", accum)
}

//	schedules a function call to be run after the function completes
func deferring() {
	first := func() {
		fmt.Println("1st")
	}

	second := func() {
		fmt.Println("2nd")
	}

	defer second()
	first()
}

func panicOrRecover() {
	defer func() {
		str := recover()
		fmt.Println(str)
	}()
	panic("PANIC")
}

// Functions ...
func Functions() {
	a, b := multiReturn()
	fmt.Println("Multiline (first,second):", a, ",", b)
	fmt.Println("Add output: ", variadicAdder(1, 2, 3))
	closure()
	panicOrRecover()
}
