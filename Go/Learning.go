package main

import (
	"fmt"

	"ebarcsa.com/learning/fundamentals"
)

func main() {
	fmt.Println("Hello, 世界")
	fundamentals.NeedYourComment()

	fundamentals.DeclarationsWithTypes()
	fundamentals.DeclarationWithAuto()
	fundamentals.Numbers()
	fundamentals.Strings()
	fundamentals.Booleans()

	fundamentals.LightningStructMe()
	fundamentals.LetsMixCwithPP()

	fundamentals.Loops()
	fundamentals.Conditionals()

	fundamentals.Functions()
}
