import Darwin

// Basic use
func hello()
{
    print("Hello World")
}

hello()

// Functions with Parameters
func magicNumber(number: Int)
{
    print("\(number) Is the magic number")
}

magicNumber(number : 5)

// Functions with return values
func doubleNumber(number: Int) -> Int {
    return number * 2
}

print("\(doubleNumber(number: 5)) is the doubled number")

// Functions with more return values (as tuples)
func getTuple(_ number: Int) -> (Int, Int) {
    return (number / 2, number * 2)
}

let myTuple = getTuple(2) // label can be missed because of _ number

// Functions as function parameters
func doTwice( function: (Int) -> (), param: Int) {
    function(param)
    function(param)
}

doTwice(function: magicNumber, param: 10)

// Inout parameters
func updateNumber(number: inout Int) {
    number += 1
}

var myNumber = 10
updateNumber(number: &myNumber) // has to be called by reference
print("myNumber value is: \(myNumber)") // prints 11

// Throwing errors
// 3 options to handle it: try, try!, try?
enum MyError: Error {
    case runtimeError(String)
}

func errorThrower() throws -> String {
    if true {
        return "True"
    } 
    else {
        // Throwing an error
        throw MyError.runtimeError("Error message")
    }
}

var result : String? = nil

do {
    result = try errorThrower()
}
catch {
    // Here you know about the error
    // Feel free to handle to re-throw
} 

result = try! errorThrower() // if an error was thrown, CRASH!

guard let newResult = try? errorThrower() else {
    // Ouch, errorThrower() threw an error.
    exit(1)
}