let CONDITION : Bool = true

// Simple if statement, brackets cannot be missed
if CONDITION {
    // CONDITION met
}

// if, else execution
if CONDITION {
    // CONDITION met
}
else {
    // CONDITION didn't met
}

// if, else-if, else execution
if CONDITION {
    // CONDITION met
}
else if !CONDITION {
    // CONDITION didn't met
}
else {
    // cannot be reached
}

// swift-case
enum CompassPoint {
    case North
    case South
    case East
    case West
}
let direction : CompassPoint = .East

/* No break is needed in cases if there is
   any executable command in it */
switch(direction)
{
    case .North:
        break
    case .South:
        break
    case .East:
        break
    case .West:
        break
    default:
        // cannot be reached
        break 
}