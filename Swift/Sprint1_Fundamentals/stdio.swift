// Print simple string
print("Hello, World!")

// Print string variables and constants
let eventName = "hackETHon"
print("Name of the event is: \(eventName)")

var timeLeft : String = "much"
print("At this moment we have \(timeLeft) time")

// Print with string interpolation
let π : Double = 3.14159
print("Number with string interpolation: \(π)")

// Prints with custom separator
print(1.0, 2.0, 3.0, 4.0, 5.0, separator: " ... ")

// Read from STDIN simply
let response = readLine()