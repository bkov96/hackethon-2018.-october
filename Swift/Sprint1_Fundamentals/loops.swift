// --- FOR LOOP

// Iterating over a range
for _ in 0..<3 {
    // do something
}

for _ in 0...2 {
    // do something
}

// Iterating in reverse
for i in (0...2).reversed() {
    print(i) // prints 2 1 0
}

// For-in loop with filtering
for i in 0..<5 where i % 2 == 0 {
    print(i)
}

// Iterating over an array or set
let participants = ["Bence, Csaba, Guan, Viktor"]

print("Participants are: ", terminator: " ")
for participant in participants{
    print(participant)
}

// Iterating over a dictionary
let ids = ["Steve" : 29,
           "Andy"  : 90,
           "Pablo" : 69]

for (name, id) in ids {
    print("\(name) has the id: \(id)")
}

// --- WHILE LOOP
var CONDITION : Bool = true

while(CONDITION) {
    CONDITION = false
}

// --- DO-WHILE
var i: Int = 0
repeat {
    print(i)
    i += 1
} while i < 3