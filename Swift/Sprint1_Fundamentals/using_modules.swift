/* In Swift you can only import module, e.g. any framework like UIKit, MapKit, etc.
   Swift classes cannot be imported as they are globally available 
   through the project when using XCode IDE.

   Import structure looks like:
      import [module]
      import [module].[submodule]
      import [import kind] [module].[symbol name] */

import class Foundation.NSString