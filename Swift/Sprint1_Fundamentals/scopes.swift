// --- NAMESPACES

/* Namespaces in Swift are not available in a way like they are eg. in C++.
   However, there is an option called Nested Types which can simulate the
   working of them. */

class Employee {
    var dept = Department()

    class Department{
        var EmpId = 150;
        var EmpName = "Suresh Dasari";

        func getDetails() -> String {
            return "Id: \(self.EmpId), Name: \(self.EmpName)"
        }
    }
}

var emp = Employee()
print(emp.dept.getDetails())

// --- SCOPES

let x = 10 // global scope
print("x outside myFunc equals \(x)")

func myFunc() {
   let x = 20 // local to myFunc
   print("x within myFunc equals: \(x)")
}