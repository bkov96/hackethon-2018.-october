// Fundamental types
let fund_String : String = "Example text"
let fund_Char : Character = "e"
let fund_Bool : Bool = true

let fund_Int : Int = Int.min
let fund_Int8 : Int8 = Int8.min
let fund_Int16 : Int16 = Int16.min
let fund_Int32 : Int32 = Int32.min
let fund_Int64 : Int64 = Int64.min

let fund_UInt : UInt = UInt.max
let fund_UInt16 : UInt16 = UInt16.max
let fund_UInt32 : UInt32 = UInt32.max
let fund_UInt64 : UInt64 = UInt64.max

let e : Float = 2.71828
let π : Double = 3.14159

// Built-int common containers
let fund_Array : Array<Int8> = [1, 3, 5, 7, 9, 11, 13, 15]

let fund_Dictionary : Dictionary<Int16, String> = [200: "OK",
                                                  403: "Access forbidden",
                                                  404: "File not found",
                                                  500: "Internal server error"]

let fund_Set : Set<String> = ["cocoa beans", "sugar", "cocoa butter", "salt"]