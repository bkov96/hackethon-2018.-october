/* Seqential execution is simple in swift,
   since new line is the separator for
   single commands, usually placed
   in a {} block */

func executeLast(){
   print("This will be executed last")
}

print("This will be executed first")
let constText = "Then execute this"
executeLast()