# About Swift

**Parent page:**
[Home page](https://gitlab.com/bkov96/hackethon-2018.-october/wikis/Home-Page)

Swift is a general-purpose, multi-paradigm, compiled programming language developed by Apple Inc. for iOS, macOS, watchOS, tvOS, and Linux [(1)](https://en.wikipedia.org/wiki/Swift_(programming_language)).
Swift is usually used with the IDE made by Apple Inc. called XCode.

## Why to learn Swift?
[(2)](http://www.bestprogramminglanguagefor.me/why-learn-swift)
* Easy to Understand
   * As a high level language, Swift reads quite close to English, so it is rather easy to pick up for beginners.
* Scalability
* Easy to Maintain
    * Swift is a statically-typed language, and xCode will check your code for errors before it builds your app. This means errors will be easier to track down.
* Fast
    * As a statically typed language, Swift is faster than dynamically typed languages because things are more clearly defined.
* Optimized Memory-Usage
    * Resource usage is an important figure for mobile apps, so Swift uses Automatic Reference Counting (ARC) that will track and manage your app’s memory usage in real time to make sure it does not take up too much memory
* Community
* Career Opportunities
  
## About the language

### Compiler
Swift compiler is available to download for **macOS** and **Linux** from the official website [(3)](https://swift.org/download/#releases).
The current version is 4.2 at the time this page was written.

### File types
The extension to be used is **.swift**. It doesn't require to distribute the source code into header and source files (like in C++) for increasing performance.

### Basic structure of the source code
The Swift source codes (just like in other languages) constist of executable code parts, whitespaces and comments. The language is not indentation-sensitive, since it uses separation block characters like { }. The whitespaces can be either spaces or tabulators.